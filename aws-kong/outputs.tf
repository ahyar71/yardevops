output "KONG_instance_id" {
  description = "ID of the KONG EC2 instance"
  value       = aws_instance.kong.id
}

output "KONG_instance_public_ip" {
  description = "Public IP address of the KONG EC2 instance"
  value       = aws_instance.kong.public_ip
}

output "KONG_instance_public_dns" {
  description = "Private DNS of the KONG EC2 instance"
  value       = aws_instance.kong.public_dns
}


output "HELLOWORLD_instance_id" {
  description = "ID of the EC2 HELLOWORLD instance"
  value       = aws_instance.hello-world.id
}

output "HELLOWORLD_instance_public_ip" {
  description = "Public IP address of the HELLOWORLD EC2 instance"
  value       = aws_instance.hello-world.public_ip
}

output "HELLOWORLD_instance_public_dns" {
  description = "Private DNS of the HELLOWORLD EC2 instance"
  value       = aws_instance.hello-world.public_dns
}