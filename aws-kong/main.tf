terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}


provider "aws" {
  profile = "default"
  region  = "us-east-2"
}

resource "aws_security_group" "sg" {
  tags = {
    type = "alltrafic-kong"
  }
}


resource "aws_instance" "kong" {
  ami           = "ami-0fa49cc9dc8d62c84"
  instance_type = "t2.micro"
  key_name   = "kong"

  user_data = <<-EOF
    #!/bin/bash
    set -ex
    sudo yum update -y
    sudo yum install git -y
    sudo amazon-linux-extras install docker -y
    sudo service docker start
    sudo usermod -a -G docker ec2-user
    sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose  
    sudo su - ec2-user
    cd /home/ec2-user
    sudo curl -LO https://gitlab.com/ahyar71/yardevops/-/archive/kong-aws-terraform-v1/yardevops-kong-aws-terraform-v1.tar.gz
    sudo tar -xzvf yardevops-kong-aws-terraform-v1.tar.gz -C /home/ec2-user

    sudo chown ec2-user kongwebapi/
    cp -R /home/ec2-user/yardevops-kong-aws-terraform-v1/check-version /home/ec2-user/kongwebapi
    cp /home/ec2-user/yardevops-kong-aws-terraform-v1/docker/docker-compose.yaml /home/ec2-user
    cp /home/ec2-user/yardevops-kong-aws-terraform-v1/docker/Dockerfile /home/ec2-user
    
    docker-compose up -d
  EOF

  

  tags = {
    Name = "kong"
  }
}

resource "aws_instance" "hello-world" {
  ami           = "ami-0fa49cc9dc8d62c84"
  instance_type = "t2.micro"
  key_name   = "kong"

  user_data = <<-EOF
    #!/bin/bash
    set -ex
    sudo yum update -y
    sudo yum install git -y
    sudo amazon-linux-extras install docker -y
    sudo service docker start
    sudo usermod -a -G docker ec2-user
    sudo su - ec2-user
    cd /home/ec2-user
    sudo curl -LO https://gitlab.com/ahyar71/yardevops/-/archive/kong-aws-terraform-v1/yardevops-kong-aws-terraform-v1.tar.gz
    sudo tar -xzvf yardevops-kong-aws-terraform-v1.tar.gz -C /home/ec2-user
    cd /home/ec2-user/yardevops-kong-aws-terraform-v1/helloworld
    docker build -t helloworld .
    docker run -d -p 80:80 helloworld
  EOF

  

  tags = {
    Name = "hello-world"
  }
}

resource "aws_network_interface_sg_attachment" "sg_attachment" {
  security_group_id    = "sg-085686c9c59e843b0"
  network_interface_id = aws_instance.kong.primary_network_interface_id
}

resource "aws_network_interface_sg_attachment" "sg_attachmenthello-world" {
  security_group_id    = "sg-085686c9c59e843b0"
  network_interface_id = aws_instance.hello-world.primary_network_interface_id
}


