return {
  no_consumer = true,
  fields = {
    allowed_version = {type = "array", required = true}
  }
}