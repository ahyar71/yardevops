local plugin = {
  PRIORITY = 1000,
  VERSION = "0.1"
}

function plugin:access(conf)
  local version = ngx.req.get_headers()["mobile-app-version"]
  local path = kong.request.get_path()

  path = path:gsub("/services/api/", "")
  path = path:gsub("/api/", "")

    local allowed_version = conf.allowed_version
    for i = 1, #allowed_version do
      if version == allowed_version[i] then
        return true
      end
    end

    return kong.response.exit(403, {message = "Forbidden"})

end

function startswith(text, prefix)
  return text:find(prefix, 1, true) == 1
end

return plugin